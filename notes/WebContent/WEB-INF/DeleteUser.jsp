<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet"  href="${pageContext.request.contextPath}/css/style.css"/>
		<title>Delete Users</title>
	</head>
<body>

<h2>Delete User</h2>

<p>${messages}</p>

	<div class="home">
		
		<a class="Home" href="${pageContext.request.contextPath}/Notes/ListUsersNotesServlet"><img src="${pageContext.request.contextPath}/img/casa.png" alt="Home"></a> 
		
		
		</div>
		


<div class="confirm">
  
  <p>Are you sure to delete your User and all of your Notes?</p>
  
  <form action="DeleteUserServlet" method="post">
   
  <input type="submit" name="submit" value="SI"><br>
  
  </form>
  
  <form action="ListUsersNotesServlet" method="get">
  
  <input type="submit" name="submit" value="NO"><br> 
  
  </form>
  
  
  </div>
	

<!-- 	<input type="button" class="button_active" value="Home" onclick="location.href='/LogoutServlet';" /> -->

</body>
</html>