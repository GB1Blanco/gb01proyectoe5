<%@ page language="java" contentType="text/html; charset=UTF-8"     pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.css" type="text/css">
</head>

<body>

  <nav class="navbar navbar-expand-lg bg-dark navbar-dark" style="">
   
    <div class="container"> <button class="navbar-toggler navbar-toggler-right border-0 p-0" type="button" data-toggle="collapse" data-target="#navbar14" style="">
        <p class="navbar-brand mb-0 text-white">
          <i class="fa d-inline fa-lg fa-stop-circle"></i> NOTES </p>
      </button>
      
	
     <div class="collapse navbar-collapse" id="navbar14">
       
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"> <a class="nav-link" href="${pageContext.request.contextPath}/Notes/ListUsersNotesServlet">Home</a> </li>
          <li class="nav-item"> <a class="nav-link" href="#">Archive</a> </li>
          <li class="nav-item"> <a class="nav-link" href="${pageContext.request.contextPath}/LogoutServlet">Log Out</a> </li>
        </ul>

        <p class="d-none d-md-block lead mb-0  text-white"> <i class="fa d-inline fa-lg fa-stop-circle"></i> <b>NOTES</b>&nbsp;</p>
        
        <ul class="navbar-nav ml-auto">
          <li class="nav-item mx-1"> <a class="nav-link" href="${pageContext.request.contextPath}/User/UpdateUserServlet">
              <i class="fa fa-github fa-fw fa-lg"></i>
            </a> </li>
          <li class="nav-item mx-1"> <a class="nav-link" href="CreateNoteServlet">
              <i class="fa fa-gitlab fa-fw fa-lg"></i>
            </a> </li>
          <li class="nav-item mx-1"> <a class="nav-link" href="TrashNoteServlet">
              <i class="fa fa-bitbucket fa-fw fa-lg"></i>
            </a> </li>
        </ul>
      
      </div>
    </div>
  </nav>





  <div class="py-2">
   
 	<div class="container">
      
	<div class="row">
       
	 <div class="col-md-12 py-1">
          <form class="form-inline w-100" action=FindNoteServlet method="post">
            <div class="input-group w-100">
              <input type="text" name="busq" class="form-control" id="inlineFormInputGroup" placeholder="Busca una nota por su título o contenido">
              <div class="input-group-append"><input class="btn btn-primary" type="submit" value="Search"></div>
            </div>
          
	   </form>
        </div>
      </div>
    </div>



    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <form class="form-inline w-100" action=ExactFindServlet method="post">
            <div class="input-group w-100">
              <input type="text" name="busq" class="form-control py-1" id="inlineFormInputGroup" placeholder="Búsqueda exacta">
              <div class="input-group-append"><input class="btn btn-primary" value="Search by keyword" type="submit"></div>
            </div>
          </form>
        </div>
      </div>
    </div>


    <div class="container">
      <div class="row">
        <div class="col-md-12 py-1">
          <div class="btn-group">
          
          <form action=FindColourServlet method="get">
		
		<select name="col">
		
  <option  value="red">red</option>
  <option value="blue" >blue</option>
  <option value="black">black</option>
  <option value="none" selected="selected">none</option>
  
</select>

    	<input type="submit" class="btn btn-primary dropdown-toggle" name="" value="Search by color">


</form>
          
            
            </div>
          </div>
        </div>
      </div>
  </div>


<div class="Principal">



		<c:forEach var="note" items="${notaspropias}">
          
          <div class=contenedor>
          
          <div class=etiqueta> 
          
          <c:choose>
							
							<c:when test="${note.first.colour=='black'}">
								<img src="${pageContext.request.contextPath}/img/black.png" alt="Empty">
							</c:when>
							<c:when test="${note.first.colour=='blue'}">
								<img src="${pageContext.request.contextPath}/img/blue.png" alt="Empty">
							</c:when>
							<c:when test="${note.first.colour=='red'}">
								<img src="${pageContext.request.contextPath}/img/red.png" alt="Empty">
							</c:when>
							<c:otherwise>
								<img src="${pageContext.request.contextPath}/img/empty.png" alt="Owner">
																
							</c:otherwise>
						</c:choose>
          
          </div>
		
		<div class=notas>
		
		<div class=title> <h2>${note.second.title}</h2> </div>
		
		    		<div class=content> <p>${note.second.content}</p> </div>
		    		
		    		<div class=actions>
		    		
		    	<a href="<c:url value='ShareNoteServlet?idn=${note.second.idn}'/>" ><img src="${pageContext.request.contextPath}/img/share.png" alt="share ${userNote.second.idn}" /></a>
		    		
		    		
						
						<form action="TrashNoteServlet" id="delete" method="post">
						
						<input type=hidden value="${note.second.idn}" name="idn">
						
						
						
						<input type="submit" name="idn" value="Borrar">
						
						</form>
						
						<a href="<c:url value='EditNoteServlet?idn=${note.second.idn}'/>" ><img src="${pageContext.request.contextPath}/img/edit.png" alt="edit ${userNote.second.idn}" /></a>
						
						<a href="<c:url value='PinNoteServlet?idn=${note.second.idn}'/>" ><img src="${pageContext.request.contextPath}/img/unpinned.png" alt="edit ${userNote.second.idn}" /></a>
						
						<a href="<c:url value='ArchiveNoteServlet?idn=${note.first.idn}'/>" ><img src="${pageContext.request.contextPath}/img/archived.png" alt="edit ${userNote.second.idn}" /></a>
						
		    		<form action="ColourNoteServlet" id="color" method="post">	
	
	
	<input type=hidden value="${note.second.idn}" name="idn">
	
	
	
    <select name=color>
    
    
    <option class="red">red</option>
    <option class="blue">blue</option>
    <option class="black">black</option>
    <option class="none">none</option>
    
    
     
    </select>	
    
    	<input type="submit" name="idn" value="select">
    	
    
    </form>	
    
    <c:choose>
							
							<c:when test="${note.first.owner==0}">
								<img src="${pageContext.request.contextPath}/img/empty.png" alt="Empty">
							</c:when>
							<c:otherwise>
								<img src="${pageContext.request.contextPath}/img/owner.png" alt="Owner">
							
							</c:otherwise>
						</c:choose>	
						
						<c:choose>
							<c:when test="${note.first.pinned==0}">
								<img src="${pageContext.request.contextPath}/img/empty.png" alt="Empty">
							</c:when>
							<c:otherwise>
								<img src="${pageContext.request.contextPath}/img/pinned.png" alt="Pinned">
							</c:otherwise>
						</c:choose>
		    		
		    		</div>
					
				</div>	
				
				</div>	

		</c:forEach>
		
		
		
        </div>
  
  </body>

</html>