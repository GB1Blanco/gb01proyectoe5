<%@ page language="java" contentType="text/html; charset=UTF-8"     pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.css" type="text/css">
</head>

<body>

  <nav class="navbar navbar-expand-lg bg-dark navbar-dark" style="">
   
    <div class="container"> <button class="navbar-toggler navbar-toggler-right border-0 p-0" type="button" data-toggle="collapse" data-target="#navbar14" style="">
        <p class="navbar-brand mb-0 text-white">
          <i class="fa d-inline fa-lg fa-stop-circle"></i> NOTES </p>
      </button>
      
	
     <div class="collapse navbar-collapse" id="navbar14">
       
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"> <a class="nav-link" href="${pageContext.request.contextPath}/Notes/ListUsersNotesServlet">Home</a> </li>
          <li class="nav-item"> <form action=ArchiveNoteServlet method="post"> 
          <input type="submit" class="nav-link" name="submit" style="
    border-top-width: 0px;
    border-left-width: 0px;
    border-bottom-width: 0px;
    border-right-width: 0px; background-color: transparent;
" value="Archive Notes"> 
          </form> </li> 
          <li class="nav-item"> <a class="nav-link" href="${pageContext.request.contextPath}/LogoutServlet">Log Out</a> </li>
        </ul>

        <p class="d-none d-md-block lead mb-0  text-white"> <i class="fa d-inline fa-lg fa-stop-circle"></i> <b>NOTES</b>&nbsp;</p>
        
        <ul class="navbar-nav ml-auto">
          <li class="nav-item mx-1"> <a class="nav-link" href="${pageContext.request.contextPath}/User/UpdateUserServlet">
              <i class="fa fa-github fa-fw fa-lg"></i>
            </a> </li>
          <li class="nav-item mx-1"> <a class="nav-link" href="CreateNoteServlet">
              <i class="fa fa-gitlab fa-fw fa-lg"></i>
            </a> </li>
          <li class="nav-item mx-1"> <a class="nav-link" href="EmptyTrashServlet">
              <i class="fa fa-bitbucket fa-fw fa-lg"></i>
            </a> </li>
        </ul>
      
      </div>
    </div>
  </nav>




	     
<div class="Principal"  style="
    margin-top: 250px;
    margin-left: 100px;
    padding-top: 0px;
    padding-right: 0px;
    margin-bottom: 250px;
    margin-right: 100px;
">

   

		<c:forEach var="notetrash" items="${notetrash}">
		
		
		<div class=notas style="
    margin-right: 20px;
" >
		
			<div class=title> <h2>${notetrash.second.title}</h2> </div>
		
		    		<div class=content> <p>${notetrash.second.content}</p> </div>
		    		
		    		<div class=actions>
					
						<c:choose>
							
							<c:when test="${notetrash.first.owner==0}">
								<img src="${pageContext.request.contextPath}/img/empty.png" alt="Empty">
							</c:when>
							<c:otherwise>
								<img src="${pageContext.request.contextPath}/img/owner.png" alt="Owner">
																
							</c:otherwise>
						</c:choose>
						

												
						<a href="<c:url value='DeleteNoteServlet?idn=${notetrash.first.idn}'/>" ><img src="${pageContext.request.contextPath}/img/delete.png" alt="delete ${userNote.second.idn}" /></a>
						
						
						<form action="TrashNoteServlet" id="delete" method="post">
						
						<input type=hidden value="${notetrash.first.idn}" name="idn">
						
						
						
						
						
						<input type="submit" name="idn" value="Recuperar">
						

		
						</form>


		</div>

</div>

		</c:forEach>
		
		
		 </div>
		 
		 
		 
		 



</body>
</html>