
package controller;

import java.io.IOException;
import java.sql.Connection;
import org.apache.commons.codec.digest.DigestUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCUserDAOImpl;
import dao.UserDAO;
import model.User;

/**
 * Servlet implementation class SignUpServlet
 */
@WebServlet(
		urlPatterns = { "/SignUpServlet"})

public class SignUpServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(HttpServlet.class.getName());

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		System.out.println("eu");
		
		System.out.println("yu");

		
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/SignUp.jsp");
		view.forward(request,response);
		
//		doPost(request,response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDao = new JDBCUserDAOImpl();
		userDao.setConnection(conn);
		
		request.setAttribute("messages","Sign Up");

		
		String username = request.getParameter("username");
		String password = request.getParameter("password");	
		String email = request.getParameter("email");		
		
		logger.info("Sign up: "+username+" - "+password+ " - "+email);
		
		User user = userDao.get(username);
		
		if(user==null){
			
			user=new User(password,email,username);
			
			
			List<String> messages = new ArrayList<String>();
			
			if (user.validate(messages)) {
				
				
				user.setPassword(DigestUtils.md5Hex(password));//la encripto
				
				
			
			user.setIdu((int) userDao.add(user));
			
			if(user.getIdu()==-1) {
				request.setAttribute("messages","Email already in use!!");
				RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/SignUp.jsp");
				view.forward(request,response);
				
			}
			
			else {
			
			HttpSession session = request.getSession();
			session.setAttribute("user",user);
			response.sendRedirect("Notes/ListUsersNotesServlet");// /orders
			
			}
			
			
			}
			
			else{
				request.setAttribute("messages",messages);
				RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/SignUp.jsp");
				view.forward(request,response);
				
				
				
			}
		}
		
		else{
		
			
			request.setAttribute("messages","Username already in use!!");
			RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/SignUp.jsp");
			view.forward(request,response);
		}

		
	}

}

