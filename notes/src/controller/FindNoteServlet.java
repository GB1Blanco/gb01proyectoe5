package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUserDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UserDAO;
import dao.UsersNotesDAO;
import model.Note;
import model.User;
import model.UsersNotes;
import util.Pair;

/**
 * Servlet implementation class FindNoteServlet
 */
@WebServlet("/Notes/FindNoteServlet")
public class FindNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);

		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);
		
		NoteDAO noteDAO = new JDBCNoteDAOImpl();
		noteDAO.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		User usr=(User) session.getAttribute("user");
		
		
		String r=request.getParameter("busq");
		
		
		List <UsersNotes> l =usersNotesDAO.getAllByUser(usr.getIdu());//sus notas(users-notes)
		
		Iterator<UsersNotes> lit = l.iterator();
		
		List <Note> k=noteDAO.getAllBySearchAll(r);//todas las notas con la busqueda
		
		List <Pair<UsersNotes, Note>> searchnotes=new ArrayList <Pair<UsersNotes, Note>> ();

		
		while(lit.hasNext()){//si hay notas mias
			
			UsersNotes n=lit.next();//pillo una
			
			
			
			
			if(k.contains(noteDAO.get(n.getIdn()))){// si coincide con la busqueda
				
				searchnotes.add(new Pair<UsersNotes, Note>(n,noteDAO.get(n.getIdn())));//me la traigo a mi lista
				
				
			}
			
			
			
		}
		
		
		session.setAttribute("notaspropias",searchnotes);
		
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/FindNote.jsp");
		view.forward(request,response);

		
		

		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

//Simulacion de implementacion del caso 16 Primer commit

//Simulacion de implementacion del caso 16 Segundo commit

