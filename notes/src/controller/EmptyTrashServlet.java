package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UsersNotesDAO;
import model.Note;
import model.User;
import model.UsersNotes;

/**
 * Servlet implementation class EmptyTrashServlet
 */
@WebServlet("/Notes/EmptyTrashServlet")
public class EmptyTrashServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmptyTrashServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	doPost(request,response);
	
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		

		HttpSession session = request.getSession();
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		UsersNotesDAO usernote=new JDBCUsersNotesDAOImpl();
		usernote.setConnection(conn);
		
		User usr=(User) session.getAttribute("user");
				
	
		
		List<UsersNotes> usersNotesList = usernote.getAllByUser(usr.getIdu());//todas las notas del pavo
		
		Iterator<UsersNotes> itUsersNotesList = usersNotesList.iterator();
		
		while(itUsersNotesList.hasNext()) {//mientras haya tuplas
			
			UsersNotes usersNotes = (UsersNotes) itUsersNotesList.next();//pillo la siguiente
			
			Note note =(Note) noteDao.get(usersNotes.getIdn());//nota
			
			
			
			if(usersNotes.getTrashed()==1){//si esta borrada
			
				
				
				if(usersNotes.getOwner()==1){//si es el propietario la borro todas las veces que aparezca en la tabla y la nota como tal
					
					usernote.delete(note.getIdn());//borrado 1
					
					noteDao.delete(note.getIdn());//borrado 2
					
					
					
				}
				
				else{//si no es propietario solo ÉL deja de verla y ya somo mu wenos saes
					
					usernote.delete(usr.getIdu(), note.getIdn());
					
				}
				
				

		}
			
			
			
			
			
		}
		
	

		
		response.sendRedirect("ListUsersNotesServlet");// voy a la lista de notas mias
		
		
		
		
	}
	
	/*
	 * Tercer commit caso de uso 15 - desarrollador junior
	 * 
	 * */
	
	
	}

