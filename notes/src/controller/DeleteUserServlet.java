package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUserDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UserDAO;
import dao.UsersNotesDAO;
import model.User;
import model.UsersNotes;

/**
 * Servlet implementation class DeleteUserServlet
 */
@WebServlet(urlPatterns = { "/User/DeleteUserServlet"})

public class DeleteUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(HttpServlet.class.getName());

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/DeleteUser.jsp");//si no login
		view.forward(request,response);		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	

HttpSession session = request.getSession();

		
		logger.info("Delete user");
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);	
		
		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		User usr=(User) session.getAttribute("user");
		
		logger.info("Eliminando a Usuario "+ usr.getUsername());
		
		
		
		
		
		LinkedList <Integer> notasmias=new LinkedList<Integer>();
		
		List<UsersNotes> l=usersNotesDAO.getAllByUser(usr.getIdu());//pillo todas sus notas y compartidas
		
		Iterator<UsersNotes> itUsersNotesList = l.iterator();
		
	
		while(itUsersNotesList.hasNext()) {//voy a separar las mias
			
			UsersNotes usersNotes = (UsersNotes) itUsersNotesList.next();
			
			if(usersNotes.getOwner()==1){//si es mia
				
				notasmias.add(usersNotes.getIdn());//la meto aqui
				
			}
				
			//si no nada
			
		}

		
		
		
		while(!notasmias.isEmpty()){//mientras tenga notas PROPIAS esta perra las voy borrando
			
			System.out.println("notas del pavo a borrar"+notasmias.size());
			
			usersNotesDAO.delete(notasmias.getFirst());//las conexiones con quien sea

			
			noteDao.delete(notasmias.getFirst());//y luego la nota
			
			System.out.println(notasmias.removeFirst().intValue());			
			
		}
		
		usersNotesDAO.delete_u(usr.getIdu());//borro todas sus conexiones con notas que queden
		
		
		if (usr!= null && userDAO.delete(usr.getIdu())) {//si hay algun user 
			
			
			
			response.sendRedirect("/notes/LogoutServlet");

		
//			request.setAttribute("messages","The User "+usr.getUsername()+" has been deleted from BD");
//
//			RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/DeleteUser.jsp");// /orders filter
//		
//			view.forward(request,response);

		}
	
		else { 
			
			request.setAttribute("messages","There are no users to delete or the operation failed!!");
			RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/DeleteUser.jsp");//si no login
			view.forward(request,response);
	}
		
		
		
	}

}