package controller;

import java.io.IOException;
import java.sql.Connection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUserDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UserDAO;
import dao.UsersNotesDAO;
import model.UsersNotes;

/**
 * Servlet implementation class ShareNoteServlet
 */
@WebServlet("/Notes/ShareNoteServlet")
public class ShareNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShareNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		HttpSession session = request.getSession();
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		
		
		String c=request.getParameter("idn");//pillo idn de nota a compartir
		

		Integer s=Integer.parseInt(c);
		
		session.setAttribute("idn", s.intValue());//lo coloco en la sesion
		
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/ShareNote.jsp");
		view.forward(request,response);
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		HttpSession session = request.getSession();
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);
		
		UserDAO userDao = new JDBCUserDAOImpl();
		userDao.setConnection(conn);
		
		String s=request.getParameter("user");
		
//		System.out.println(s);
		
		
		if(userDao.get(s)!=null){//si existe
			
			int idu=userDao.get(s).getIdu();
			int idn=(Integer)session.getAttribute("idn");
			
			
			UsersNotes userNote=new UsersNotes(idu,idn,0,0,0,0,"S/N");//se la comparto
			
			usersNotesDAO.add(userNote);
		}
		
		else{
			
			request.setAttribute("messages","Wrong username!!");
			RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/ShareNote.jsp");
			view.forward(request,response);
			
			

		}
		
		
		response.sendRedirect("ListUsersNotesServlet");
		
		
		
		
		
		
		
	}

}