package controller;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.*;
import util.*;
import dao.*;


import javax.servlet.RequestDispatcher;

import java.sql.Connection;



/**
 * Servlet implementation class ListUsersNotesServlet
 */
@WebServlet("/Notes/ListUsersNotesServlet")
public class ListUsersNotesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(HttpServlet.class.getName());
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListUsersNotesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		Connection conn = (Connection) getServletContext().getAttribute("dbConn");
		UserDAO userDAO = new JDBCUserDAOImpl();
		userDAO.setConnection(conn);

		UsersNotesDAO usersNotesDAO = new JDBCUsersNotesDAOImpl();
		usersNotesDAO.setConnection(conn);
		
		NoteDAO noteDAO = new JDBCNoteDAOImpl();
		noteDAO.setConnection(conn);
		
		HttpSession session = request.getSession();
		
		User usr=(User) session.getAttribute("user");
		
		logger.info("list notes servlet");
		
		
		
//		List<UsersNotes> usersNotesList = usersNotesDAO.getAllByUser(usr.getIdu());//lista de usernotes de toda las notas accesibles por el idU
//		
//		Iterator<UsersNotes> itUsersNotesList = usersNotesList.iterator();
//		
//		List<Triplet<User, UsersNotes, Note >> userNoteTripletList = new ArrayList<Triplet<User, UsersNotes, Note>>();
//						
//		while(itUsersNotesList.hasNext()) {//mientras haya tuplas
//			
//			UsersNotes usersNotes = (UsersNotes) itUsersNotesList.next();//creo una nueva
//			User user = userDAO.get(usersNotes.getIdu());//pillo user que tiene acceso
//			Note note = noteDAO.get(usersNotes.getIdn());// nota
//			userNoteTripletList.add(new Triplet<User, UsersNotes, Note>(user,usersNotes,note));
//			
//		}
//		request.setAttribute("usersNotes",userNoteTripletList);
		
		
		List <UsersNotes> usersNotesList = usersNotesDAO.getAllByUser(usr.getIdu());//lista de usernotes de toda las notas accesibles por el idU
		System.out.println("Taam de la notas accesibles por el idU "+usersNotesList.size());
//		
		Iterator<UsersNotes> itUsersNotesList = usersNotesList.iterator();
		
//		List<Note> notaspropias = new ArrayList< Note>();
		
		List<Pair<UsersNotes, Note>> notaspropias = new ArrayList<Pair<UsersNotes, Note>>();//creo lista de usernote/note

		List<Pair<UsersNotes, Note>> notaspin = new ArrayList<Pair<UsersNotes, Note>>();//creo lista de usernote/note

//		List<Pair<UsersNotes, Note>> notaspin = new ArrayList<Pair<UsersNotes, Note>>();//creo lista de usernote/note

		
//		List<Pair<User,List<Pair<UsersNotes, Note>>>> usersPairPairList = new ArrayList<Pair<User,List<Pair<UsersNotes, Note>>>>();
		
		while(itUsersNotesList.hasNext()) {
			
//			logger.info(new Integer(itUsersNotesList.next().getIdn()).toString());
			
			UsersNotes usersNotes = (UsersNotes) itUsersNotesList.next();//creo una nueva id idn owner...
			
			Note note = noteDAO.get(usersNotes.getIdn());// pillo nota suya
			
			
			logger.info(new Integer(note.getIdn()).toString());

			
			if(usersNotes.getTrashed()==0) {//si no esta borrada
			
			if(usersNotes.getArchived()==0){//si no esta archivada
			
			if(usersNotes.getPinned()==1){
				
				notaspin.add(new Pair<UsersNotes, Note>(usersNotes, note));//añado un nuevo par de elmtos a la lista de pines
				
				
				
			}
//			
			else{
				
				
//			
			notaspropias.add(new Pair<UsersNotes, Note>(usersNotes, note));//añado un nuevo par de elmtos
//			
				
			
			
			}
			}

			}
			
					
		}
		
		request.setAttribute("notaspropias",notaspropias);
		
		request.setAttribute("notaspin",notaspin);

		
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/ListUsersNotes.jsp");
		view.forward(request,response);
		
	
	}

	
}

 
//Simulaci�n de implementaci�n del caso de uso 9, Primer commit
//Simulaci�n de implementaci�n del caso de uso 9, Segundo commit
//Simulaci�n de implementaci�n del caso de uso 9, tercer commit


//Simulacion de Implementacion del Caso de Uso 8, Primer Commit 
//Simulacion de Implementacion del Caso de Uso 8, Segundo Commit
//Simulacion de Implementacion del Caso de Uso 8, tercer Commit 

