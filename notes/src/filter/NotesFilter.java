package filter;

import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Logger;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.JDBCNoteDAOImpl;
import dao.JDBCUsersNotesDAOImpl;
import dao.NoteDAO;
import dao.UsersNotesDAO;
import model.User;

/**
 * Servlet Filter implementation class NotesFilter
 */
@WebFilter(dispatcherTypes = {DispatcherType.REQUEST }
,          urlPatterns = { "/Notes/*" })

//"/Notes/EditNoteServlet", "/Notes/ShareNoteServlet"



public class NotesFilter implements Filter {
	
	private static final Logger logger = Logger.getLogger(Filter.class.getName());

	
	private FilterConfig config;

    /**
     * Default constructor. 
     */
    public NotesFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		
		
		HttpServletRequest req = (HttpServletRequest) request;
	    HttpServletResponse res = (HttpServletResponse) response;
		
	    HttpSession session = ((HttpServletRequest)request).getSession(true);
	    
//	    ServletContext context = config.getServletContext();

		
		
		Connection conn = (Connection) config.getServletContext().getAttribute("dbConn");
		
		NoteDAO noteDao = new JDBCNoteDAOImpl();
		noteDao.setConnection(conn);
		
		UsersNotesDAO usernote=new JDBCUsersNotesDAOImpl();
		usernote.setConnection(conn);
		
		if(request.getParameter("idn")!=null) {
		
		String c=request.getParameter("idn");//id de la nota a hacer lo que sea
		
		logger.info(c);

		
		Integer s=Integer.parseInt(c);

		
		User usr=(User) session.getAttribute("user");
		
		
		
		logger.info("Notesfilter");

		
		logger.info("buscando"+ s);
		
		if(usernote.get(usr.getIdu(), s)!=null) {

												//si la encuentro
				
				logger.info("Encontrada"+s);
				
				chain.doFilter(request, response);

				
				
			}
			
	
		
		else {//No tiene acceso asique a chuparla
			
			
			logger.info("No enc redirijo");

			
			
			request.setAttribute("messages","Unauthorized access request");

			RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/ListUsersNotes.jsp");//si no login
			view.forward(request,response);		
			
		
		}}
		
		else {				chain.doFilter(request, response);
}
		
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
		
		 this.config = fConfig;
	}

}
