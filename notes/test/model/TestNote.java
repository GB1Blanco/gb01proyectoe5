package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import model.Note;



public class TestNote {
	
	

	
	@Test
	public void testCons() {
		
		Note n=null;
		
		assertTrue(n==null);
		
		n=new Note();
		
		assertTrue(n!=null);
		
		
	}
	
	@Test
	public void testConsParam() {
		
		Note n=null;
		
		assertTrue(n==null);		
		n=new Note("aaaaaa","bbbbbbbbbb");
		
		assertTrue(n!=null);
		assertTrue(!n.getTitle().isEmpty());
		assertTrue(!n.getContent().isEmpty());

	}
	

	@Test
	public void testValidate() {
		
		Note n1=new Note();
		
		n1.setIdn(0);
		n1.setContent("example1");
		n1.setTitle("exa");
		
		
		
		Note n2=new Note();
		
		n2.setIdn(1);
		n2.setContent("exampleeeee1");
		n2.setTitle("example");
		
		
	
		assertEquals(false,n1.validate(new ArrayList<>()));
		
		assertEquals(true,n2.validate(new ArrayList<>()));

		
		
			
	}

	@Test
	public void testGetSetIdn() {
		
		Note n=new Note();
		
		n.setIdn(0);
		
		assertNotNull(n.getIdn());		
		
		
		
	}



	@Test
	public void testGetSetContent() {
		

		Note n=new Note();
		
		n.setContent("xuavuavu");
		
		assertNotNull(n.getContent());		
		
	}

	@Test
	public void testSetGetTitle() {

		Note n=new Note();
		
		n.setTitle("xuavuavu");
		
		assertNotNull(n.getTitle());		
	}



	

	@Test
	public void testEqualsObject() {
		

		Note n=new Note();
		
		n.setIdn(0);
		
		Note n2=new Note();
		
		assertTrue(n.equals(n2));		
		
		
	}

}
